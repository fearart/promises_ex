import { Post } from './models/post.model';
import { User } from './models/user.model';

export class App {
    constructor(
        private time: number,
        private url: string
    ) {
        this
            .init()
            .then(([x, y]) => console.log(x, y));
    }

    async init() {
        return Promise
            .all(
                [this.getPosts(), this.getUserData()]
            );
    }

    async getPosts(): Promise<Post[]> {
        const response = await fetch(this.url);
        return response.json();
    }

    async getUserData(): Promise<User> {
        return new Promise(resolve => setTimeout(
            resolve,
            this.time,
            {
                id: '34njcv-34uuj34-34mnj34-343mkm3-3434mkm', 
                name: 'John Doe'
            }
        ));
    }

}
export default App;